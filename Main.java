import java.util.Scanner;

public class Main{
  public static void main(String[] args){
    Scanner s = new Scanner(System.in); //Cria um objeto do tipo Scanner para receber entradas pelo terminal.

    System.out.println("digite o seu nome"); //imprime na tela uma mensagem.
    String nome = s.nextLine(); //guarda na variável nome a próxima linha digitada no terminal (classe Scanner ajuda a fazer isso)

    System.out.println("digite o seu sobrenome"); //imprime na tela uma mensagem.
    String sobrenome = s.nextLine(); //guarda na variável sobrenome a próxima linha digitada no terminal (classe Scanner ajuda a fazer isso)

    System.out.println("digite sua idade"); //imprime na tela uma mensagem.
    int idade = s.nextInt(); //guarda na variável idade o próximo inteiro digitado no terminal (classe Scanner ajuda a fazer isso)

    Pessoa p = new Pessoa(); //cria um objeto do tipo pessoa.
    p.nome = nome; //copia o valor de nome para o atributo nome do objeto associado a variável p.
    p.sobrenome = sobrenome; //copia o valor de sobrenome para o atributo sobrenome do objeto associado a variável p.
    p.idade = idade; //copia o valor de idade para o atributo idade do objeto associado a variável p.

    System.out.println(); //pula linha
    System.out.println(p.nome+" "+p.sobrenome); //informa nome e sobrenome colocados no objeto associado a variável p.

    if(p.idade > 10){ //verifica se a idade informada é maior que um valor.
      System.out.println("bem vindo"); //imprime na tela uma mensagem.
    }else{
      System.out.println("ops"); //imprime na tela uma mensagem.
    }
  }
}
